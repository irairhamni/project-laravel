<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'IndexController@home');    
Route::get('/register', 'AuthController@register');
Route::post('/welcome2', 'AuthController@send');

route::get('/data-table', function(){
    return view('table.datatable');
});

route::get('/table', function(){
    return view('table.table');
});
