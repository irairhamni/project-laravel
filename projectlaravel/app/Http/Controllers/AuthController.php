<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }
    public function welcome2(){
        return view('page.welcome2');
    }
    public function send(Request $request){
        $nama1 = $request['nama1'];
        $nama2 = $request['nama2'];
        return view('page.welcome2', compact('nama1','nama2'));
    }
}
